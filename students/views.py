from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import *
from .models import *

def index(request):
	if request.method == "POST":
		form = TokenForm(request.POST)
		if form.is_valid():
			submitted_token = form.cleaned_data["token"]
			try:
				Tokens.objects.get(value = submitted_token)
			except:
				return HttpResponseRedirect("/failure")
			response = HttpResponseRedirect("/submit")
			response.set_cookie("token", submitted_token)
			return response
	else:
		form = TokenForm()

	return render(request, "index.html", {"form": form})

def failure(request):
	return render(request, "failure.html")

def success(request):
	return render(request, "success.html")

def submit(request):
	if request.method == "POST":
		form = StudentsForm(request.POST)
		if form.is_valid():
			submitted_token = request.COOKIES.get("token")
			response = HttpResponseRedirect("/success")
			try:
				Tokens.objects.get(value = submitted_token).delete()
				response.delete_cookie("token")
			except:
				return HttpResponseRedirect("/failure")
			form.save()
			return response
	else:
		form = StudentsForm()

	return render(request, "submit.html", {"form": form})

