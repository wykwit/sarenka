from django.contrib import admin
from .models import *


class StudentsAdmin(admin.ModelAdmin):
	list_display = ("login", "year", "letter", "nr", "real_name")

admin.site.register(Tokens)
admin.site.register(Students, StudentsAdmin)
