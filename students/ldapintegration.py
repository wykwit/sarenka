from ldap3 import Server, Connection, ALL
from django.conf import settings


def check_login_availability(login: str) -> bool:
    if settings.LDAP_DISABLED:
        return True
    server = Server(settings.LDAP_SERVER, get_info=ALL)
    connection = Connection(server,
                            str().join(['cn={},'.format(settings.LDAP_USER), settings.LDAP_BASE]),
                            settings.LDAP_PASSWORD,
                            auto_bind=True)
    return not connection.search(settings.LDAP_BASE, '(cn={})'.format(login))
