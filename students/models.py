from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator, RegexValidator, MinLengthValidator,\
    MaxLengthValidator, ValidationError
from uuid import uuid4
from hashlib import sha256
from datetime import date
from . import blacklist


def current_year():
    return int(date.today().strftime("%Y"))


def validate_blacklist(value):
    if value in blacklist.usernames:
        raise ValidationError('Login jest niedostępny.')

def validate_first_char_alpha(value):
    if not value[0].isalpha():
        raise ValidationError('Login musi zaczynać się od litery.')


class Students(models.Model):
    real_name = models.CharField(max_length=32, blank=False, verbose_name='Pełne imię i nazwisko')
    login = models.CharField(max_length=12, blank=False, unique=True,
                             verbose_name='Proponowana nazwa użytkownika (login)',
                             help_text='Nazwa konta musi zaczynać się od litery i składać się z minimum dwóch (2) i '
                                       'maksimum dwunastu (12) znaków. Może zawierać tylko małe litery bez polskich '
                                       'znaków i cyfry.',
                             validators=[
                                 validate_first_char_alpha,
                                 RegexValidator(regex=r'^([0-9a-z]+)+$', message='Nazwa użytkownika może zawierać '
                                                                                      'tylko małe litery bez polskich '
                                                                                      'znaków i cyfry'),
                                 MinLengthValidator(2, message='Nazwa użytkownika jest za krótka'),
                                 MaxLengthValidator(12, message='Nazwa użytkownika jest za długa'),
                                 validate_blacklist,
                             ])
    year = models.PositiveIntegerField(default=current_year,
                                       verbose_name='Rok przyjęcia',
                                       validators=[MinValueValidator(2000), MaxValueValidator(2099)])
    letter = models.CharField(max_length=1, blank=False, verbose_name='Litera klasy',
                              validators=[
                                  MaxLengthValidator(1, message='Błędna litera klasy'),
                                  RegexValidator(regex=r'[a-zA-Z]', message='Błędna litera klasy'),
                              ])
    nr = models.PositiveSmallIntegerField(verbose_name='Numer w dzienniku',
                                          validators=[MaxValueValidator(50, message='Niepoprawny numer')])
    submit_date = models.DateField(auto_now_add=True)
    email = models.EmailField(max_length=64, blank=True, verbose_name='Adres e-mail',
                              help_text='Na niego będą przekazywane maile wysyłane na twój szkolny adres.')

    def __str__(self):
        return self.login

    class Meta:
        verbose_name_plural = "students"


def generate_token():
    return sha256(uuid4().bytes).hexdigest()[::8]


class Tokens(models.Model):
    value = models.CharField(max_length=8, unique=True, default=generate_token)
    birth = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.value)

    class Meta:
         verbose_name_plural = "tokens"

