from django import forms
from django.forms import models
from .models import Students
from students.ldapintegration import check_login_availability
import re


class StudentsForm(models.ModelForm):
	class Meta:
		model = Students
		exclude = ["submit_date"]

	def clean(self):
		cleaned_data = super().clean()
		login = cleaned_data.get('login')

		if login and not check_login_availability(login):
			self.add_error('login', 'Ta nazwa użytkownika jest już zajęta.')


class TokenForm(forms.Form):
	token = forms.CharField(max_length=8)
