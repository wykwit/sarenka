from django.core.management.base import BaseCommand
from students.models import Tokens


class Command(BaseCommand):
	help = "Generate given ammount of tokens."

	def add_arguments(self, parser):
		parser.add_argument('ammount', type=int)

	def handle(self, *args, **options):
		for i in range(0, options["ammount"]):
			token = Tokens()
			print(token)
			token.save()

