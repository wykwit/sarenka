from django.core.management.base import BaseCommand
from students.models import Tokens


class Command(BaseCommand):
	help = "Print out all active tokens."

	def handle(self, *args, **options):
		for token in Tokens.objects.all():
			print(token)

