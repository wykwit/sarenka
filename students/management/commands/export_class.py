from django.core.management.base import BaseCommand, CommandError
from students.models import Students
from django.db.models import Q


class Command(BaseCommand):
    help = 'Exports students from a given class'

    def add_arguments(self, parser):
        parser.add_argument('class_letter', type=str)


    def handle(self, *args, **options):
        if len(options['class_letter']) != 1:
            raise CommandError('Incorrect class letter')

        class_students = Students.objects.filter(
            Q(letter=options['class_letter']) | Q(letter=options['class_letter'].swapcase()),
        ).order_by('nr')

        for class_student in class_students:
            uid = str(
                (class_student.year % 10**2 + 20) * 10**3 +
                (ord(class_student.letter.lower()) - 96) * 10**2 +
                class_student.nr
            )
            gid = str(
                10 * 10**3 +
                class_student.year % 10**2 * 10 +
                ord(class_student.letter.lower()) - 96
            )

            line = ':'.join([class_student.login, uid, gid, class_student.real_name, class_student.email])

            print(line)
